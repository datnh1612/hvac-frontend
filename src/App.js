import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/auth.service";

import Login from "./components/login/components/login.component";
import Register from "./components/login/components/register.component";

import BoardUser from "./components/role-based/components/board-user.component";
import BoardAdmin from "./components/role-based/components/board-admin.component";
import Energy from "./components/energy/energy.component";

import AhuConditionRuleSite from "./components/rule/site/ahu_conditional_rule.site";
import AhuParametricRuleSite from "./components/rule/site/ahu_parametric_rule.site";
import AhuSatOptimizationSite from "./components/rule/site/ahu_sat_optimization.site"
import AHU_tempNcop from "./components/rule/site/ahu_overview_temp_and_cop";


class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      showAdminBoard: false,
      currentUser: undefined
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showAdminBoard: user.roles.includes("ROLE_ADMIN")
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showAdminBoard } = this.state;

    return (
      <Router>
        <div>
            <Switch>
              <Route exact path={["/"]} component={AHU_tempNcop} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/ahu" component={AHU_tempNcop} />
              <Route path="/user" component={BoardUser} />
              <Route path="/admin" component={BoardAdmin} />
              <Route path="/energy" component={Energy}></Route>
              <Route exact path="/rule" component={AhuConditionRuleSite} />
              <Route exact path="/rule_param" component={AhuParametricRuleSite} />
              <Route exact path="/ai" component={AhuSatOptimizationSite} />
            </Switch>
          </div>
      </Router>
    );
  }
}

export default App;