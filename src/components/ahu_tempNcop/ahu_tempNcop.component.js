// This is a public page that shows public default content
import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./ahu_tempNcop.css"
import Brand from "../../asset/logo.JPG";
import GreenLightIcon from "../../asset/green-light.png";
import Fan from "../../asset/fan.png";
import Setting from "../../asset/setting.png";
import Notification from "../../asset/notification.png";
import Message from "../../asset/message.png";
import UserIcon from "../../asset/admin.png";
import LogOutIcon from "../../asset/logout.png";
import HamBurGerButton from "../../asset/hamburger-button.png";
import Chart from "../chart/chart.component";
import AHUDataHeating from "../../db/dummy-heating.csv";
import AHUDataCooling from "../../db/dummy-cooling.csv";
import COPData from "../../db/dummy-cop.csv";
import OverviewService from "../../services/overview.service";
import Loader from 'react-loader-spinner'

export default class AHU_tempNcop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      AHUTempData: [],
      AHUCopData: [],
      loading: true,
      TempData: AHUDataHeating,
      status: "heating"
    };
    this.coolingHandleClick = this.coolingHandleClick.bind(this);
    this.heatingHandleClick = this.heatingHandleClick.bind(this);
  }

  getData() {
    var tableName = "HVAC_ahu_cooling"; //HVAC_ahu_heating
    var tempParams = ["ra_t_c", "space_t_c", "sa_t_c", "oa_t_c"];
    var cop = "dt_air_k"
    OverviewService.getOverview(tableName, tempParams, cop).then(response => {
      var tempData = this.transformTempData(response.tempData);
      var copData = this.transformCopData(response.copData);
      return {
        temp: tempData,
        cop: copData
      }
    });
  }

  transformCopData(copData) {
    var slices = [];
    var COPlist = [];
    copData.forEach(element => {
      COPlist.push({
        date: new Date(parseInt(element.timestamp)),
        measurement: parseInt(element.ra_t_c)
      });
    });

    slices.push({
      id: "AHU COP",
      values: COPlist
    });

    return slices;
  }

  transformTempData(tempData) {
    var slices = [];
    var RATCList = [];
    var SATCList = [];
    var SpaceTCList = [];
    var OATCList = [];
    tempData.forEach(element => {
      RATCList.push({
        date: new Date(parseInt(element.timestamp)),
        measurement: parseInt(element.ra_t_c)
      });

      SATCList.push({
        date: new Date(parseInt(element.timestamp)),
        measurement: parseInt(element.sa_t_c)
      });

      SpaceTCList.push({
        date: new Date(parseInt(element.timestamp)),
        measurement: parseInt(element.space_t_c)
      });

      OATCList.push({
        date: new Date(parseInt(element.timestamp)),
        measurement: parseInt(element.oa_t_c)
      });
    });

    slices.push({
      id: "SA T, C",
      values: SATCList
    });
    slices.push({
      id: "RA T, C",
      values: RATCList
    });
    slices.push({
      id: "Space T, C",
      values: SpaceTCList
    });
    slices.push({
      id: "OA T, C",
      values: OATCList
    });
    return slices;
  }

  coolingHandleClick() {
    this.setState({
      TempData: AHUDataCooling,
      status: "cooling"
    });
    this.forceUpdate()
  }

  heatingHandleClick() {
    this.setState({
      TempData: AHUDataHeating,
      status: "heating"
    });
    this.forceUpdate()
  }


  render() {
    var result = this.getData();
    console.log(result);
    return (
      <div>
        <div className="wrapper row">
          <div id="content" className="row">
            <div id="content-title">
              <div id="title-line">AHU 01 Dashboard </div>
              <div id="mode-content" className="row">
                <div>Mode:</div>
                <div id="cooling" onClick={this.coolingHandleClick}>Cooling</div>
                <div> | </div>
                <div id="heating" onClick={this.heatingHandleClick}>Heating</div>
              </div>
            </div>
            <div>
              <div id="chart-left" className="col-6">
                <Chart data={this.state.TempData} classNameOfDivCover="chart-left" ></Chart>
              </div>
              <div id="chart-right" className="col-6">
                <Chart data={COPData} classNameOfDivCover="chart-right"></Chart>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}