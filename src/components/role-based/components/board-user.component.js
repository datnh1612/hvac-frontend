import React, { Component } from "react";

import UserService from "../../../services/user.service";

export default class BoardUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    UserService.getUserBoard().then(
      response => {
        var userMap = [];
        response.data.forEach(user => {
          userMap.push(user.email);
        });
        this.setState({
          content: userMap
        });
      },
      error => {
        this.setState({
          content:
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
          <h3>{this.state.content.toString()}</h3>
        </header>
      </div>
    );
  }
}