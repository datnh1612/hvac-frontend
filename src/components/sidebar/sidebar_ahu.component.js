import Breadcrumbs from '@trendmicro/react-breadcrumbs';
import { Button, ButtonGroup } from '@trendmicro/react-buttons';
import Dropdown, { MenuItem } from '@trendmicro/react-dropdown';
import ensureArray from 'ensure-array';
import React, { PureComponent } from 'react';
import styled from 'styled-components';
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import Navigation from '../navigation/navigation.component';
import '@trendmicro/react-sidenav/dist/react-sidenav.min.css';
import 'font-awesome/css/font-awesome.css'

const Main = styled.main`
    position: relative;
    overflow: hidden;
    transition: all .15s;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
`;

export default class SideBarEnergy extends PureComponent {
    state = {
        selected: 'home',
        expanded: false
    };

    onSelect = (selected) => {
        this.setState({ selected: selected });
    };

    onToggle = (expanded) => {
        this.setState({ expanded: expanded });
        this.props.onToggle(expanded);
    };

    pageTitle = {
        'home': 'Home',
        'devices': ['Devices'],
        'reports': ['Reports'],
        'settings/policies': ['Settings', 'Policies'],
        'settings/network': ['Settings', 'Network']
    };

    renderBreadcrumbs() {
        const { selected } = this.state;
        const list = ensureArray(this.pageTitle[selected]);

        return (
            <Breadcrumbs>
                {list.map((item, index) => (
                    <Breadcrumbs.Item
                        active={index === list.length - 1}
                        key={`${selected}_${index}`}
                    >
                        {item}
                    </Breadcrumbs.Item>
                ))}
            </Breadcrumbs>
        );
    }

    navigate = (pathname) => () => {
        this.setState({ selected: pathname });
    };

    render() {
        const { expanded, selected } = this.state;

        return (
            <div>
                <SideNav onSelect={this.onSelect} onToggle={this.onToggle}>
                    <SideNav.Toggle />
                    <SideNav.Nav selected={selected}>
                        <NavItem eventKey="ahu">
                            <NavIcon></NavIcon>
                            <NavText style={{ paddingRight: 0, fontSize: '1.25em' }} title="AHU">
                                AHU
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="ahu01">
                            <NavIcon>
                                <i className="fa fa-fw fa-line-chart" style={{ fontSize: '1.75em' }} />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="AHU_01">
                                AHU 01
                            </NavText>
                            <NavItem eventKey="ahu01/dashboard">
                                <NavText title="AHU_01_dashboard">
                                    Dashboard
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/define_conditional_rule">
                                <NavText title="AHU_01_define_conditional_rule">
                                    Conditional Rules
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/define_parametric_rule">
                                <NavText title="AHU_01_define_parametric_rule">
                                    Parametric Rules
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/ai_weather_prediction">
                                <NavText title="AHU_01_ai_weather_prediction">
                                    AI Weather Prediction
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/ai_occupant_prediction">
                                <NavText title="AHU_01_ai_occupant_prediction">
                                    AI Occupant Prediction
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/ai_optimization/sa_t_ptimization">
                                <NavText title="AHU_01_sa_t_ptimization">
                                    SA T Optimization
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/ai_optimization/sa_fan_ptimization">
                                <NavText title="AHU_01_sa_fan_ptimization">
                                    SA Fan Optimization
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu01/ai_optimization/oa_ptimization">
                                <NavText title="AHU_01_oa_ptimization">
                                    OA Optimization
                                </NavText>
                            </NavItem>
                        </NavItem>
                        <NavItem eventKey="ahu02">
                            <NavIcon>
                                <i className="fa fa-fw fa-line-chart" style={{ fontSize: '1.75em' }} />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="AHU_02">
                                AHU 02
                            </NavText>
                            <NavItem eventKey="ahu02/dashboard">
                                <NavText title="AHU_02_dashboard">
                                    Dashboard
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/define_conditional_rule">
                                <NavText title="AHU_02_define_conditional_rule">
                                    Conditional Rules
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/define_parametric_rule">
                                <NavText title="AHU_02_define_parametric_rule">
                                    Parametric Rules
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/ai_weather_prediction">
                                <NavText title="AHU_02_ai_weather_prediction">
                                    AI Weather Prediction
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/ai_occupant_prediction">
                                <NavText title="AHU_02_ai_occupant_prediction">
                                    AI Occupant Prediction
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/ai_optimization/sa_t_ptimization">
                                <NavText title="AHU_02_sa_t_ptimization">
                                    SA T Optimization
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/ai_optimization/sa_fan_ptimization">
                                <NavText title="AHU_02_sa_fan_ptimization">
                                    SA Fan Optimization
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="ahu02/ai_optimization/oa_ptimization">
                                <NavText title="AHU_02_oa_ptimization">
                                    OA Optimization
                                </NavText>
                            </NavItem>
                        </NavItem>
                    </SideNav.Nav>
                </SideNav>
                {/* <Main expanded={expanded}>
                    <Navigation></Navigation>
                    {this.renderBreadcrumbs()}
                </Main> */}
            </div>
        );
    }
}