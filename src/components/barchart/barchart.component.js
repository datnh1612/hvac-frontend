import React, { Component } from "react";
import "./chart.css";
import "bootstrap/dist/css/bootstrap.min.css";
import * as d3 from "d3";


export default class BarChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initRender: true
        }
        this.chartRef = React.createRef();
        this.drawChart = this.drawChart.bind(this);
    }

    drawChart() {
        const width = 390;
        const height = 360;
        const margin = 5;
        const padding = 5;
        const adj = 30;
        // Clear svg
        d3.select("div#" + this.props.classNameOfDivCover).select("svg").remove();
        // we are appending SVG first
        const svg = d3.select("div#" + this.props.classNameOfDivCover).append("svg")
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", "-"
                + adj + " -"
                + adj + " "
                + (width + adj * 3) + " "
                + (height + adj * 3))
            .style("padding", padding)
            .style("margin", margin)
            .classed("svg-content", true);

        //-----------------------------DATA------------------------------//

        const timeConv = d3.timeParse("%m/%d/%Y %H:%M");
        const dataset = d3.csv(this.props.data);

        dataset.then(function (data) {
            const slices = data.columns.slice(1).map(function (id) {
                return {
                    id: id,
                    values: data.map(function (d) {
                        return {
                            date: timeConv(d.Date),
                            measurement: +d[id]
                        };
                    })
                };
            });

            console.log("Column headers", data.columns);
            // console.log("Column headers without date", data.columns.slice(1));
            // returns the sliced dataset
            console.log("Slices", slices);
            // returns the first slice
            // console.log("First slice", slices[0]);
            // returns the array in the first slice
            // console.log("A array", slices[0].values);
            // returns the date of the first row in the first slice
            // console.log("Date element", slices[0].values[0].date);
            // returns the array's length
            // console.log("Array length", (slices[0].values).length);
            //----------------------------SCALES-----------------------------//
            // create domain and range which data will display
            const xScale = d3.scaleTime().range([0, width]);
            const yScale = d3.scaleLinear().rangeRound([height, 0]);
            xScale.domain(d3.extent(data, function (d) {
                return timeConv(d.Date)
            }));
            yScale.domain([(0), d3.max(slices, function (c) {
                return d3.max(c.values, function (d) {
                    return d.measurement + 4;
                });
            })
            ]);
            // construct a line

            const line = d3.line()
                .x(function (d) { return xScale(d.date); })
                .y(function (d) { return yScale(d.measurement); })
                .curve(d3.curveMonotoneX);
            // create 2 axes
            const yaxis = d3.axisLeft()
                .scale(yScale);
            const xaxis = d3.axisBottom()
                .tickFormat(d3.timeFormat('%m-%d'))
                .scale(xScale);

            // separates lines 
            let id = 0;
            const ids = function () {
                return "line-" + id++;
            }
            //-----------------------------AXES------------------------------//

            //----------------------------LINES------------------------------//

            //-------------------------2. DRAWING----------------------------//
            svg.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xaxis);

            svg.append("g")
                .attr("class", "axis")
                .call(yaxis);
            // select data for y axis

            const lines = svg.selectAll("lines")
                .data(slices)
                .enter()
                .append("g");

            lines.append("path")
                .attr("class", ids)
                .attr("d", function (d) { return line(d.values); });
            //-----------------------------AXES------------------------------//
            //----------------------------LINES------------------------------//
            // display infor of lines
            lines.append("text")
                .attr("class", "serie_label")
                .datum(function (d) {
                    return {
                        id: d.id,
                        value: d.values[d.values.length - 1]
                    };
                })
                .attr("transform", function (d) {
                    return "translate(" + (width)
                        + "," + (yScale(d.value.measurement)) + ")";
                })
                .attr("x", 5)
                .text(function (d) { return d.id; });

        });
    }
    componentDidMount() {
        this.drawChart();
    }

    componentDidUpdate() {
        this.drawChart();
    }


    render() {
        return <div className="rowChart" ref={this.myRef} />;
    }
}