// This is a public page that shows public default content
import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./energy.css"
import Brand from "../../asset/logo.JPG";
import GreenLightIcon from "../../asset/green-light.png";
import Fan from "../../asset/fan.png";
import Setting from "../../asset/setting.png";
import Notification from "../../asset/notification.png";
import Message from "../../asset/message.png";
import UserIcon from "../../asset/admin.png";
import LogOutIcon from "../../asset/logout.png";
import HamBurGerButton from "../../asset/hamburger-button.png";
import Chart from "../chart/chart.component";
import AHUDataHeating from "../../db/dummy-heating.csv";
import AHUDataCooling from "../../db/dummy-cooling.csv";
import COPData from "../../db/dummy-cop.csv";
import Loader from 'react-loader-spinner'

export default class Energy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      AHUTempData: [],
      AHUCopData: [],
      loading: true,
      TempData: AHUDataHeating,
      status: "heating"
    };
    this.coolingHandleClick = this.coolingHandleClick.bind(this);
    this.heatingHandleClick = this.heatingHandleClick.bind(this)
  }


  coolingHandleClick() {
    this.setState({
      TempData: AHUDataCooling,
      status: "cooling"
    });
    this.forceUpdate()
  }

  heatingHandleClick() {
    this.setState({
      TempData: AHUDataHeating,
      status: "heating"
    });
    this.forceUpdate()
  }


  render() {
    return (
      <div>
              <nav style={{ backgroundColor: "#A9A9A9" }} className="navbar navbar-expand-lg navbar-light">
                <div className="navbarItem">
                  <img src={HamBurGerButton} id="sidebarCollapse" className="item-image"></img>
                </div>
                <a className="navbar-brand" href="#" id="navbarBrand">
                  <div>
                    <img src={Brand} id="brand-image">
                    </img>
                    <span id="brand-text">Quatro HVAC FDD Predictions AI Optimization</span>
                  </div>
                </a>
                <div className="col-2"></div>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul className="navbar-nav mr-auto">
                    <li className="nav-item active" className="navbarItem">
                      <img src={GreenLightIcon} className="item-image"></img>
                      <a className="nav-link navbarItemText" href="energy">Energy</a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                      <img src={Fan} className="item-image"></img>
                      <a className="nav-link navbarItemText" href="ahu">AHU</a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                      <img src={Setting} className="item-image"></img>
                      <a className="nav-link navbarItemText" href="#">Heat Pump</a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                      <img src={Notification} className="item-image"></img>
                      <a className="nav-link navbarItemText" href="#"></a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                      <img src={Message} className="item-image"></img>
                      <a className="nav-link navbarItemText" href="#"></a>
                    </li>
                    <li className="nav-item dropdown" className="navbarItem">
                      <img src={UserIcon} className="item-image"></img>
                      <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                 </a>
                      <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a className="dropdown-item" href="#">Action</a>
                        <a className="dropdown-item" href="#">Another action</a>
                        <div className="dropdown-divider"></div>
                        <a className="dropdown-item" href="#">Something else here</a>
                      </div>
                    </li>
                    <li className="nav-item" className="navbarItem">
                      <img src={LogOutIcon} className="item-image"></img>
                      <a className="nav-link disabled" href="#">Log out</a>
                    </li>
                  </ul>
                  {/* <form class="form-inline my-2 my-lg-0">
               <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
               <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
             </form> */}
                </div>
              </nav>
              <div className="wrapper row">
                <div id="sidebar">
                  <div id="sidebar-title" className="row">
                    <img src={Fan} className="item-image col-3"></img>
                    <div className="col-3">AHU</div>
                  </div>
                  <div id="sidebar-content">
                    <div>AHU1</div>
                    <div>AHU2</div>
                  </div>

                </div>
                <div id="content" className="row">
                  <div>
                    <div id="chart-left" className="col-6">
                      <Chart data={this.state.TempData} classNameOfDivCover="chart-left" ></Chart>
                    </div>
                    <div id="chart-right" className="col-6">
                      <Chart data={COPData} classNameOfDivCover="chart-right"></Chart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    );
  }
}