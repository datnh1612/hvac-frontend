import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./navigation.css" 
import Brand from "../../asset/logo.JPG";
import GreenLightIcon from "../../asset/green-light.png";
import Fan from "../../asset/fan.png";
import Setting from "../../asset/setting.png";
import Notification from "../../asset/notification.png";
import Message from "../../asset/message.png";
import UserIcon from "../../asset/admin.png";
import LogOutIcon from "../../asset/logout.png";
import HamBurGerButton from "../../asset/hamburger-button.png";


export default class Navigation extends Component {
    constructor(props) {
      super(props);
    };

    render() {
        return (
            <div>
                <nav style={{ backgroundColor: "#A9A9A9" }} className="navbar navbar-expand-lg navbar-light">
                <a className="navbar-brand" href="#" id="navbarBrand">
                    <div>
                    <img src={Brand} id="brand-image">
                    </img>
                    <span id="brand-text">Quatro HVAC FDD Predictions AI Optimization</span>
                    </div>
                </a>
                <div className="col-2"></div>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                    <li className="nav-item active" className="navbarItem">
                        <img src={GreenLightIcon} className="item-image"></img>
                        <a className="nav-link navbarItemText" href="#">Energy</a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                        <img src={Fan} className="item-image"></img>
                        <a className="nav-link navbarItemText" href="#">AHU</a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                        <img src={Setting} className="item-image"></img>
                        <a className="nav-link navbarItemText" href="#">Heat Pump</a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                        <img src={Notification} className="item-image"></img>
                        <a className="nav-link navbarItemText" href="#"></a>
                    </li>
                    <li className="nav-item" className="navbarItem">
                        <img src={Message} className="item-image"></img>
                        <a className="nav-link navbarItemText" href="#"></a>
                    </li>
                    <li className="nav-item dropdown" className="navbarItem">
                        <img src={UserIcon} className="item-image"></img>
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a className="dropdown-item" href="#">Action</a>
                        <a className="dropdown-item" href="#">Another action</a>
                        <div className="dropdown-divider"></div>
                        <a className="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li className="nav-item" className="navbarItem">
                        <img src={LogOutIcon} className="item-image"></img>
                        <a className="nav-link disabled" href="#">Log out</a>
                    </li>
                    </ul>
                    {/* <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form> */}
                </div>
                </nav>
            </div>
        )
    }
}