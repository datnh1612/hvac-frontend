import React, { PureComponent } from 'react';
import './rule.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


class DisplayCoolingRules extends PureComponent {

    constructor(props) {
        super(props);
    
        this.state = {
            content: "",
            apiResponse: [],
            sa_t_c: "",
            sa_t_c_pred: ""
        };
    }

    callAPI() {
        fetch("http://localhost:9090/api/get_ahu_fan_cooling_predict")
            .then(res => res.json())
            .then((result) => {
                let body = result.body;
                body = JSON.parse(body);
                this.setState({
                    apiResponse : [
                        {
                            "title": "dt Air, K",
                            "value": body["dt_air_k"]
                        },
                        {
                            "title": "Space T, C",
                            "value": body["space_t_c"]
                        },
                        {
                            "title": "Occupancy, %",
                            "value": body["occupancy"]
                        },
                        {
                            "title": "SA Fan, Hz",
                            "value": body["sa_fan_freq_hz"]
                        }
                    ],
                    sa_t_c : body["sa_t_c"],
                    sa_t_c_pred : body["pred"]
                });
            })
    }    

    componentWillMount() {
        this.callAPI();
    }

    render() {
        return (
        <div className="row">
            <div className="columnLeft">
                <table>
                    <thead>
                        <tr>
                            <td>
                                <span><b>Parameters</b></span>
                            </td>
                            <td>
                                <span><b>Telemetry</b></span>
                            </td>
                            <td>
                                <span><b>Conditions for HIGHER Set Point Reset</b></span>
                            </td>
                            <td>
                                <span><b>Conditions for LOWER Set Point Reset</b></span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.apiResponse.map((value, index) => (
                            <tr key={index}>
                                <td>
                                    <span>{value.title}</span>
                                </td>
                                <td>
                                    <div className="boxed">
                                        <span className="ml-cell">{value.value}</span>
                                    </div>
                                </td>
                                <td>
                                    <div className="boxed">
                                        <span className="ml-cell">Set by Machine Learning</span>
                                    </div>
                                </td>
                                <td>
                                    <div className="boxed">
                                        <span className="ml-cell">Set by Machine Learning</span>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <br></br>
            </div>
            <div className="columnRight">
                <table>
                    <thead>
                        <tr>
                            <td>
                                <span><b>SA T Current Set Point</b></span>
                            </td>
                            <td>
                                <span><b>SA T Optimized Set Point</b></span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div className="boxed side">
                                    <span className="ml-cell">{this.state.sa_t_c}</span>
                                </div>
                            </td>
                            <td>
                                <div className="boxed side">
                                    <span className="ml-cell">{this.state.sa_t_c_pred}</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        )
    }
}

class DisplayHeatingRules extends PureComponent {

    constructor(props) {
        super(props);
    
        this.state = {
            content: "",
            apiResponse: [],
            sa_t_c: "",
            sa_t_c_pred: ""
        };
    }

    callAPI() {
        fetch("http://localhost:9090/api/get_ahu_fan_heating_predict")
            .then(res => res.json())
            .then((result) => {
                let body = result.body;
                body = JSON.parse(body);
                this.setState({
                    apiResponse : [
                        {
                            "title": "dt Air, K",
                            "value": body["dt_air_k"]
                        }
                    ],
                    sa_t_c : body["sa_t_c"],
                    sa_t_c_pred : body["pred"]
                });
            })
    }    

    componentWillMount() {
        this.callAPI();
    }

    render() {
        return (
        <div className="row">
            <div className="columnLeft">
                <table>
                    <thead>
                        <tr>
                            <td>
                                <span><b>Parameters</b></span>
                            </td>
                            <td>
                                <span><b>Telemetry</b></span>
                            </td>
                            <td>
                                <span><b>Conditions for HIGHER Set Point Reset</b></span>
                            </td>
                            <td>
                                <span><b>Conditions for LOWER Set Point Reset</b></span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.apiResponse.map((value, index) => (
                            <tr key={index}>
                                <td>
                                    <span>{value.title}</span>
                                </td>
                                <td>
                                    <div className="boxed">
                                        <span className="ml-cell">{value.value}</span>
                                    </div>
                                </td>
                                <td>
                                    <div className="boxed">
                                        <span className="ml-cell">Set by Machine Learning</span>
                                    </div>
                                </td>
                                <td>
                                    <div className="boxed">
                                        <span className="ml-cell">Set by Machine Learning</span>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <br></br>
            </div>
            <div className="columnRight">
                <table>
                    <thead>
                        <tr>
                            <td>
                                <span><b>SA T Current Set Point</b></span>
                            </td>
                            <td>
                                <span><b>SA T Optimized Set Point</b></span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div className="boxed side">
                                    <span className="ml-cell">{this.state.sa_t_c}</span>
                                </div>
                            </td>
                            <td>
                                <div className="boxed side">
                                    <span className="ml-cell">{this.state.sa_t_c_pred}</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        )
    }
}

export default class Rule extends PureComponent {

    render() {
        return (
            <div>
                <h3>AHU 01 AI SA Fan Optimization</h3>
                <div>
                    <h4>Mode: <span className="cooling">Cooling</span></h4>
                    <h4>Direct AI Optimization by Set Point Reset</h4>
                    <h5><span className="ai-title">Direct AI Optimization by Set Point Reset</span></h5>
                    <DisplayCoolingRules></DisplayCoolingRules>
                </div>
                <div>
                    <h4>Mode: <span className="heating">Heating</span></h4>
                    <h4>Direct AI Optimization by Set Point Reset</h4>
                    <h5><span className="ai-title">Direct AI Optimization by Set Point Reset</span></h5>
                    <DisplayHeatingRules></DisplayHeatingRules>
                </div>
            </div>
        );
    }
}
