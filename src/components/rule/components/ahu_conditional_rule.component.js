import React, { PureComponent } from 'react';
import './rule.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


class DisplayRules extends PureComponent {

    constructor(props) {
        super(props);
    
        this.state = {
            content: "",
            apiResponse: []
        };
    }

    render() {
        return (
        <div>
            <table>
                <thead>
                    <tr>
                        <td>
                            <span><b>Conditional FDD Rules Entry</b></span>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <span><b>Conditional FDD Rules Names</b></span>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select id="rule_1" name="rule_1" className="input">
                                <option value="" disabled defaultValue>Select rule</option>
                                <option value="chw_tin_c">CHW Tin, C</option>
                                <option value="chw_tout_c">CHW Tout, C</option> 
                                <option value="chw_valve_pos_pct">CHW Valve Position, %</option>
                                <option value="chw_fr_ls">CHW FR, L/s</option>
                                <option value="sa_fan_freq_hz">SA Fan Frequency, Hz</option>
                                <option value="ra_fan_freq_hz">RA Fan Frequency, Hz</option>
                                <option value="sa_t_c">SA T, C</option>
                                <option value="ra_t_c">RA T, C</option>
                                <option value="dt_air_k">dT Air, K</option>
                                <option value="sa_fr_kgs">SA FR, kg/s</option>
                                <option value="ra_fr_kgs">RA FR, kg/s</option>
                                <option value="sa_duct_st_press_pa">SA Duct St Pressure, Pa</option>
                                <option value="oa_fr_kgs">OA FR, kg/s</option>
                                <option value="oa_damper_open_pct">OA Damper Open, %</option>
                                <option value="space_co2">Space, CO2</option>
                                <option value="space_rh_rh">Space RH, RH</option>
                                <option value="space_t_c">Space T, C</option>
                                <option value="oa_t_c">OA T, C</option> 
                                <option value="oa_rh_rh">OA RH, RH</option> 
                                <option value="ahu_cop_ai_opt">AI Optimization: AHU COP</option>
                                <option value="fan_power_kw">Fan Power, KW</option>
                                <option value="energy_weekly">Energy Weekly, KW-Hr</option> 
                                <option value="energy_monthly">Energy Monthly, KW-Hr</option>
                                <option value="energy_yearly">Energy Yearly, KW-Hr</option>
                                <option value="occupancy">OCCUPANCY</option>
                            </select>
                        </td>
                        <td>
                            <select>
                                <option value="" disabled defaultValue>Select operation</option>
                                <option value="larger_than">&gt;</option>
                                <option value="smaller_than">&lt;</option> 
                                <option value="equal">=</option>
                                <option value="larger_or_equal">&ge;</option>
                                <option value="smaller_or_equal">&le;</option>
                            </select>
                        </td>
                        <td>
                            <input id="rule_1_value" name="rule_1_value" type="text" className="input" />
                        </td>
                        <td>
                            <select>
                                <option value="" disabled defaultValue>Select logical operation</option>
                                <option value="and_op">And</option>
                                <option value="or_op">Or</option> 
                            </select>
                        </td>
                        <td>
                            <input id="rule_name" name="rule_name" type="text" className="input" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select id="rule_1" name="rule_1" className="input">
                                <option value="" disabled defaultValue>Select rule</option>
                                <option value="chw_tin_c">CHW Tin, C</option>
                                <option value="chw_tout_c">CHW Tout, C</option> 
                                <option value="chw_valve_pos_pct">CHW Valve Position, %</option>
                                <option value="chw_fr_ls">CHW FR, L/s</option>
                                <option value="sa_fan_freq_hz">SA Fan Frequency, Hz</option>
                                <option value="ra_fan_freq_hz">RA Fan Frequency, Hz</option>
                                <option value="sa_t_c">SA T, C</option>
                                <option value="ra_t_c">RA T, C</option>
                                <option value="dt_air_k">dT Air, K</option>
                                <option value="sa_fr_kgs">SA FR, kg/s</option>
                                <option value="ra_fr_kgs">RA FR, kg/s</option>
                                <option value="sa_duct_st_press_pa">SA Duct St Pressure, Pa</option>
                                <option value="oa_fr_kgs">OA FR, kg/s</option>
                                <option value="oa_damper_open_pct">OA Damper Open, %</option>
                                <option value="space_co2">Space, CO2</option>
                                <option value="space_rh_rh">Space RH, RH</option>
                                <option value="space_t_c">Space T, C</option>
                                <option value="oa_t_c">OA T, C</option> 
                                <option value="oa_rh_rh">OA RH, RH</option> 
                                <option value="ahu_cop_ai_opt">AI Optimization: AHU COP</option>
                                <option value="fan_power_kw">Fan Power, KW</option>
                                <option value="energy_weekly">Energy Weekly, KW-Hr</option> 
                                <option value="energy_monthly">Energy Monthly, KW-Hr</option>
                                <option value="energy_yearly">Energy Yearly, KW-Hr</option>
                                <option value="occupancy">OCCUPANCY</option>
                            </select>
                            </td>
                        <td>
                            <select>
                                <option value="" disabled defaultValue>Select operation</option>
                                <option value="larger_than">&gt;</option>
                                <option value="smaller_than">&lt;</option> 
                                <option value="equal">=</option>
                                <option value="larger_or_equal">&ge;</option>
                                <option value="smaller_or_equal">&le;</option>
                            </select>
                        </td>
                        <td>
                            <input id="rule_2_value" name="rule_2_value" type="text" className="input" />
                        </td>
                        <td>
                            <select>
                                <option value="" disabled defaultValue>Select logical operation</option>
                                <option value="and_op">And</option>
                                <option value="or_op">Or</option> 
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select id="rule_1" name="rule_1" className="input">
                                <option value="" disabled defaultValue>Select rule</option>
                                <option value="chw_tin_c">CHW Tin, C</option>
                                <option value="chw_tout_c">CHW Tout, C</option> 
                                <option value="chw_valve_pos_pct">CHW Valve Position, %</option>
                                <option value="chw_fr_ls">CHW FR, L/s</option>
                                <option value="sa_fan_freq_hz">SA Fan Frequency, Hz</option>
                                <option value="ra_fan_freq_hz">RA Fan Frequency, Hz</option>
                                <option value="sa_t_c">SA T, C</option>
                                <option value="ra_t_c">RA T, C</option>
                                <option value="dt_air_k">dT Air, K</option>
                                <option value="sa_fr_kgs">SA FR, kg/s</option>
                                <option value="ra_fr_kgs">RA FR, kg/s</option>
                                <option value="sa_duct_st_press_pa">SA Duct St Pressure, Pa</option>
                                <option value="oa_fr_kgs">OA FR, kg/s</option>
                                <option value="oa_damper_open_pct">OA Damper Open, %</option>
                                <option value="space_co2">Space, CO2</option>
                                <option value="space_rh_rh">Space RH, RH</option>
                                <option value="space_t_c">Space T, C</option>
                                <option value="oa_t_c">OA T, C</option> 
                                <option value="oa_rh_rh">OA RH, RH</option> 
                                <option value="ahu_cop_ai_opt">AI Optimization: AHU COP</option>
                                <option value="fan_power_kw">Fan Power, KW</option>
                                <option value="energy_weekly">Energy Weekly, KW-Hr</option> 
                                <option value="energy_monthly">Energy Monthly, KW-Hr</option>
                                <option value="energy_yearly">Energy Yearly, KW-Hr</option>
                                <option value="occupancy">OCCUPANCY</option>
                            </select>
                        </td>
                        <td>
                            <select>
                                <option value="" disabled defaultValue>Select operation</option>
                                <option value="larger_than">&gt;</option>
                                <option value="smaller_than">&lt;</option> 
                                <option value="equal">=</option>
                                <option value="larger_or_equal">&ge;</option>
                                <option value="smaller_or_equal">&le;</option>
                            </select>
                        </td>
                        <td>
                            <input id="rule_3_value" name="rule_3_value" type="text" className="input" />
                        </td>
                        <td>
                            <input type="button" value="Go" className="button-submit"/>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br></br>
        </div>
        )
    }
}

export default class Rule extends PureComponent {

    constructor(props) {
        super(props);
    
        this.state = {
            content: "",
            apiResponse: []
        };
    }

    callAPI() {
        fetch("http://localhost:9090/api/test_db_all")
            .then(res => res.json())
            .then((result) => {
                this.setState({apiResponse: result})
            })
            // .then(res => res.text())
    }    

    componentWillMount() {
        this.callAPI();
    }

    render() {
        return (
            <div>
                <div>
                    <h3>AHU 01 Define Conditional Rules</h3>
                    <h4>Algorithmic FDD Execution Conditional Rules</h4>
                </div>
                <div>
                    <Tabs>
                        <TabList>
                            <Tab>Heating Mode</Tab>
                            <Tab>Cooling Mode</Tab>
                        </TabList>   
                        <TabPanel>
                            <DisplayRules></DisplayRules>
                            <table>
                                <thead>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <h6>Status</h6>
                                        </td>
                                        <td>
                                            <h6>Action</h6>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.apiResponse.map((value, index) => (
                                        <tr key={index}>
                                            <td className="rule_display">{value.name}</td>
                                            <td className="rule_display"><span>Active</span></td>
                                            <td>
                                                <input type="button" value="Delete" className="button-submit"/>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </TabPanel>
                        <TabPanel>
                            <DisplayRules></DisplayRules>
                            <table>
                                <thead>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <h6>Status</h6>
                                        </td>
                                        <td>
                                            <h6>Action</h6>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.apiResponse.map((value, index) => (
                                        <tr key={index}>
                                            <td className="rule_display">{value.name}</td>
                                            <td className="rule_display"><span>Active</span></td>
                                            <td>
                                                <input type="button" value="Delete" className="button-submit"/>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </TabPanel>
                    </Tabs>  
                </div>
            </div>
        );
    }
}
