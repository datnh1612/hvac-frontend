import React, { PureComponent } from 'react';
import 'react-tabs/style/react-tabs.css';
import Navigation from '../../navigation/navigation.component';
import Sidebar from '../../sidebar/sidebar_ahu.component';
import Rule from '../components/ahu_conditional_rule.component';
import styled from 'styled-components';


const Main = styled.main`
    position: relative;
    overflow: hidden;
    transition: all .15s;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
`;

export default class AhuConditionalRule extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            expanded: ""
        };
    
        this.onToggle = this.onToggle.bind(this);
    }
    
    onToggle(expanded) {
        this.setState({ expanded: expanded });
    }

    render() {
        const { expanded } = this.state;
        return (
            <div>
                <Sidebar onToggle={this.onToggle}></Sidebar>
                <Main expanded={expanded}>
                    <Navigation></Navigation>
                    <Rule></Rule>
                </Main>
            </div>
        );
    }
}
