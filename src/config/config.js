class Config {
    AUTHENTICATION_URL = "http://localhost:8080/api/auth/";
    USER_URL = "http://localhost:8080/api/test/";
    HVAC_URL = "http://localhost:8080/api/hvac/";
    OVERVIEW_URL = "http://localhost:8080/api/overview/"
}
export default new Config();