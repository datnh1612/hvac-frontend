import axios from 'axios';
import authHeader from './auth-header';
import Config from '../config/config';

const OVERVIEW_URL = Config.OVERVIEW_URL;

class HVACService {
    getOverview(tableName, tempParams, cop) {
        return axios.post(
            OVERVIEW_URL + 'data',
            {
                tableName,
                tempParams,
                cop
            }
        ).then(response => {
            return response.data
        });
    }
}

export default new HVACService();