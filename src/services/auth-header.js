// In the case user access protected resources, the HTTP request needs Authorization header.
// This service get token from localstorage and add it in to header for next requests.

export default function authHeader() {
    const user = JSON.parse(localStorage.getItem('user'));
  
    if (user && user.accessToken) {
      return { 
        'x-access-token': user.accessToken
      };
    } else {
      return {};
    }
  }